#!/bin/bash

cd /code/core
if [ "x$DJANGO_MIGRATE_DATABASE" = 'xon' ]; then
	python manage.py makemigrations --noinput
	python manage.py migrate --noinput
fi

if [ "x$DJANGO_LOAD_INITIAL_DATA" = 'xon' ]; then
	# Run here a custom inicialization script like `python manage.py load_initial_data`
	python manage.py createsuperuser --no-input
fi

if [[ ${ENV_TYPE} = "dev" ]]; then
	python manage.py runserver ${GUNICORN_BIND}
else
	gunicorn core.wsgi --workers ${GUNICORN_WORKERS} --max-requests ${GUNICORN_MAX_REQUESTS} --timeout ${GUNICORN_TIMEOUT} --bind ${GUNICORN_BIND} --log-level=${GUNICORN_LOG_LEVEL} --log-file=${GUNICORN_LOG_FILE} 
fi

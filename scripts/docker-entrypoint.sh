#!/bin/sh
set -e

cd /code
if [ ! -f /code/core/manage.py ]; then
    cd core
    echo "Downloading wagtail custom project template ..."
    curl --header "Private-Token: ${WAGTAIL_CUSTOM_TEMPLATE_REPO_TOKEN}" "${WAGTAIL_CUSTOM_TEMPLATE_URL}" --output template.zip
    echo "Wagtail project not found. Creating..."
    coderedcms start --sitename ${HOST} --domain ${HOST} --template template.zip core . > /dev/null
    rm template.zip
    sed '/#psycopg2/s/^#//' -i /code/core/requirements.txt
    pip install --no-cache-dir -r /code/core/requirements.txt
else
    echo "Wagtail project found. Skip creating process."
    pip install --no-cache-dir -r /code/core/requirements.txt
fi

if [ -f /code/extra-requirements.txt ]; then
    echo "Installing extra requirements ..."
    pip install --no-cache-dir -r /code/extra-requirements.txt
fi

cd /code/core
if [ "x$DJANGO_COLLECTSTATICS" = 'xon' ]; then
    python manage.py collectstatic --noinput
fi

exec "$@"

# This must be the last line as runserver takeover shell control until SIGINT
>&2 /scripts/wait-for-it.sh db:5432 --strict -- /scripts/runserver.sh
# docker-coderedcms

Docker environment that creates a website using [WagtailCRX](https://github.com/coderedcorp/coderedcms)

## Setup

**Requirements:** [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/install/) (Docker Compose is included with Docker Desktop).

Open a terminal and follow those instructions:

```sh
# 1. Decide where to put the project. We use "~/Projects" in our examples.
cd ~/Projects
# 2. Clone the docker-wagtailcrx repository.
git clone https://gitlab.com/adremides/docker-wagtailcrx.git
# 3. Move inside the new folder.
cd docker-wagtailcrx/
# 4. Modify .env and docker-compose.yml files as needed
# 5. Build
docker-compose build
```

Once the build is complete:

```sh
# 6. Start your containers and wait for them to finish their startup scripts.
docker-compose up
# Success!
```

- Visit your site at http://localhost
- The Wagtail's admin interface is at http://localhost/admin/ - log in with `admin` / `Admin.1234` (or the DJANGO_SUPERUSER_USERNAME and DJANGO_SUPERUSER_PASSWORD variables in .env file).

## What you can do

### See a list of running containers

```sh
$ docker-compose ps
      Name                    Command                  State                    Ports
---------------------------------------------------------------------------------------------------
db-wagtailcrx      docker-entrypoint.sh postg ...   Up (healthy)   5432/tcp
nginx-wagtailcrx   /docker-entrypoint.sh ngin ...   Up             0.0.0.0:80->80/tcp,:::80->80/tcp
web-wagtailcrx     /scripts/docker-entrypoint.sh    Up
```

### Run tests

```sh
coming soon
```

### Customize User Administrator

Add this variables to docker-compose.yml in Web service, environment section

```yml
services:
  web:
    ...
    environment:
      ...
      DJANGO_SUPERUSER_USERNAME: 'admin'
      DJANGO_SUPERUSER_FIRST_NAME: 'administrator'
      DJANGO_SUPERUSER_LAST_NAME: 'root'
      DJANGO_SUPERUSER_EMAIL: 'admin@$localhost'
      DJANGO_SUPERUSER_PASSWORD: 'Admin.1234'
      ...
```

### Customize Gunicorn parameters

Add this variables to docker-compose.yml in Web service, environment section

```yml
services:
  web:
    ...
    environment:
      ...
      GUNICORN_WORKERS: 8
      GUNICORN_MAX_REQUESTS: 1024
      GUNICORN_TIMEOUT: 180
      GUNICORN_BIND: '0.0.0.0:8000'
      GUNICORN_LOG_LEVEL: debug
      GUNICORN_LOG_FILE: -
      ...
```

### You can open a django shell session

```sh
docker-compose exec web python manage.py shell
```

### You can open a shell on the web server

```sh
docker-compose exec web bash
```

### Export data from database

```sh
docker-compose exec web python manage.py dumpdata --natural-foreign --natural-primary -e contenttypes -e auth.Permission --indent 2 > dump.json
```

### Import data from fixture

```sh
docker-compose exec web python manage.py loaddata "path/to/fixture/file"
```

### Upgrade Coderedcms package

```sh
docker-compose exec web pip install --force-reinstall --no-deps coderedcms
```

### Run Dev environment

Make sure to modify the variable ENV_TYPE so ENV_TYPE=dev in .env file. Then run docker-compose with the dev yml.

```sh
docker-compose -f docker-compose.yml -f docker-compose-dev.yml up --build --detach
```

## Getting ready to contribute

Here we will make a guide of how to contribute to this project and the projects involved.

## TODO

- Tests

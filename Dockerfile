# Use an official Python runtime as a parent image
FROM python:3.8-bullseye
LABEL maintainer="yjaskolowski@altec.com.ar"

# Set Python environment varibles
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

# Set Django default environment variables
ENV DJANGO_SETTINGS_MODULE=core.settings.prod
ENV DJANGO_DEBUG=off
ENV DJANGO_SECRET_KEY=CHANGE_THIS__CHANGE_THIS__CHANGE_THIS__CHANGE_THIS
ENV DJANGO_SUPERUSER_USERNAME=admin
ENV DJANGO_SUPERUSER_FIRST_NAME=Administrator
ENV DJANGO_SUPERUSER_LAST_NAME=Root
ENV DJANGO_SUPERUSER_EMAIL=admin@localhost
ENV DJANGO_SUPERUSER_PASSWORD=Admin.1234
ENV WAGTAIL_CUSTOM_TEMPLATE_REPO_TOKEN=None
ENV WAGTAIL_CUSTOM_TEMPLATE_URL=https://gitlab.com/adremides/wagtailcrx_template/-/archive/main/wagtailcrx_template-main.zip

# Set Gunicorn default environment variables
ENV GUNICORN_WORKERS=8
ENV GUNICORN_MAX_REQUESTS=1024
ENV GUNICORN_TIMEOUT=180
ENV GUNICORN_BIND=0.0.0.0:8000
ENV GUNICORN_LOG_LEVEL=debug
ENV GUNICORN_LOG_FILE=-

# Install OS's dependencies
RUN apt-get update -y && apt-get install --yes \
    postgresql-client \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
    && rm -rf /var/lib/apt/lists/*

# Web host
ARG HOST=localhost

# User info
ARG USER=docker
ARG UID=1000
ARG GID=1000
# default password for user
ARG PW=docker

# Using unencrypted password/ specifying password
RUN useradd -m ${USER} --uid=${UID} && \
    echo "${USER}:${PW}" | chpasswd

ENV PATH="/home/${USER}/venv/bin:$PATH"

# Create working directory & give user permission
RUN mkdir -p /code && \
    chown -R ${USER}:${UID} /code

USER ${UID}:${GID}

RUN python -m venv /home/${USER}/venv

# Install WagtailCRX (aka Coderedcms)
RUN python -m pip install --upgrade pip wheel setuptools \
    && pip install --no-cache-dir \
    coderedcms \
    django_sendmail_backend \
    gunicorn

WORKDIR /code/

# Download Wagtail Custom Template
RUN if [ ! "$WAGTAIL_CUSTOM_TEMPLATE_URL" = "DEFAULT" ]; \
    then curl --header "Private-Token: ${WAGTAIL_CUSTOM_TEMPLATE_REPO_TOKEN}" "${WAGTAIL_CUSTOM_TEMPLATE_URL}" --output template.zip; \
    fi

# Create inicial project
RUN if [ "$WAGTAIL_CUSTOM_TEMPLATE_URL" = "DEFAULT" ]; \
    then coderedcms start --sitename ${HOST} --domain ${HOST} core > /dev/null; \
    else coderedcms start --sitename ${HOST} --domain ${HOST} --template template.zip core > /dev/null; rm template.zip; \
    fi

# Uncomment psycopg2 line in requirements.txt and install it
RUN sed '/#psycopg2/s/^#//' -i /code/core/requirements.txt
RUN pip install --no-cache-dir -r /code/core/requirements.txt

# Create media directories with user permissions (static files
#  should be created during collectstatic in entrypoint script)
RUN mkdir -p /code/core/media

WORKDIR /code/core
VOLUME ["/code/core"]

# Copy entrypoint file
COPY ./scripts/docker-entrypoint.sh /scripts/docker-entrypoint.sh
COPY ./scripts/wait-for-it.sh /scripts/wait-for-it.sh
COPY ./scripts/runserver.sh /scripts/runserver.sh
USER root
RUN chmod +x /scripts -R
USER ${UID}:${GID}

# Here we can add some extra Python modules needed for the project
# (examples: wagtail addons, apps to customize templates, etc.)
COPY ./extra-requirements.txt /code/extra-requirements.txt

# Using a wrapper script to allow us to easily add more commands to container startup:
ENTRYPOINT ["/scripts/docker-entrypoint.sh"]
